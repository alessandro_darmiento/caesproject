DIM EQU 208
STUD EQU 30

.MODEL SMALL
.STACK
.DATA

DECKS DB DIM DUP (?)  

TOKENS DB ?
SENIORITY DB ?
OMP_FLAG DB ? 

MASKS DB 00110000b
MASKV DB 00001111b

CARDS DB 3 DUP (?)

FINAL_SCORE DB ?
HONOR_FLAG DB ?
LUCKY_FLAG DB 0 

DECK_INDEX DW 0
CARDS_INDEX DW 0   

SUPER_FLAG DB 0

;STUFF FOR INPUT/OUTPUT

STR1 DB 10, 13, 'Enter number of token: ','$',  
STR2 DB 10, 13, 'Enter your seniority: ','$'
STR3 DB 10, 13, 'Are you a student of professor OMP (Y/N)? ','$' 
  
STR4 DB 10, 13, 'Do you want to draw another card (Y/N)? ', '$'
STR5 DB 10, 13, 'You just drawn: ','$' 

STR6 DB 10, 13, 'Do you want to take advantage of super rule (Y/N)? ','$'    
STR7 DB 10, 13, 'Do you want to take advantage of extra rule (Y/N)? ','$'
STR8 DB 10, 13, 'Do you want to take advantage of additional rule (Y/N)? ','$'  

STR9 DB 10, 13, 'Your final grade is: ','$'
STR10 DB ' with Honor!', '$' 
STR11 DB 10, 13, 'You are rejected! ','$'

INPUT DB 0  
OUTPUT DW 0
OUTC DB 0

;END IO



.CODE
.STARTUP


;SOME INIT VALUES

MOV DECKS[0], 00110001b
MOV DECKS[1], 00100010b
MOV DECKS[2], 11011011b
MOV DECKS[3], 10001001b
MOV DECKS[4], 10101101b
MOV DECKS[5], 11010111b
MOV DECKS[6], 10000111b
MOV DECKS[7], 01110111b
MOV DECKS[8], 00110001b
MOV DECKS[9], 00100010b
MOV DECKS[10], 11011011b
MOV DECKS[11], 10001001b
MOV DECKS[12], 10101101b
MOV DECKS[13], 11010111b
MOV DECKS[14], 10000111b
MOV DECKS[15], 01110111b


;MOV OMP_FLAG, 0
;MOV SENIORITY, 4
;MOV TOKENS, 3




;END INIT


XOR BP, BP

MOV CARDS_INDEX, 0

loop_label:

;READ TOKENS, SENIORITY, OMP_FLAG 



MOV OUTPUT, OFFSET STR1
CALL PRINTF
CALL SCANF
MOV AL, INPUT
MOV TOKENS, AL

MOV OUTPUT, OFFSET STR2
CALL PRINTF
CALL SCANF
MOV AL, INPUT
MOV SENIORITY, AL

MOV OUTPUT, OFFSET STR3
CALL PRINTF
CALL SCANF 
CALL YNTO10
MOV AL, INPUT
MOV OMP_FLAG, AL


MOV SUPER_FLAG, 0
MOV AL, TOKENS
XOR AH, AH

MOV SI, AX
XOR DI, DI

MOV CARDS_INDEX, 0 ; ERROR!! FORGOT TO RE-INITIALIZE CARDS_INDEX

draw_label:

CMP DI, 0
JE no_choice  


;READ WANNA_DRAW
MOV OUTPUT, OFFSET STR4
CALL PRINTF
CALL SCANF    
CALL YNTO10
MOV AL, INPUT
;

CMP AL, 0

JE go_computation

no_choice:
CALL DRAW
INC DI
DEC SI
CMP SI, 0
JNZ draw_label

go_computation:

CMP OMP_FLAG, 1
JNE no_lucky_student

CALL CHECK_LUCK
CMP LUCKY_FLAG, 1
JNE no_lucky_student

MOV FINAL_SCORE, 30
MOV HONOR_FLAG, 1
JMP end_student

no_lucky_student:
CMP SENIORITY, 1
JE first_year
CMP SENIORITY, 2
JE second_year
CMP SENIORITY, 3
JE third_year

fourth_fifth_year:

;READ WANNA USE SUPER_RULE
MOV OUTPUT, OFFSET STR6
CALL PRINTF
CALL SCANF 
CALL YNTO10
MOV AL, INPUT
;

CMP AL, 0
JE third_year
CALL SUPER_RULE

third_year:

;READ WANNA USE EXTRA_RULE
MOV OUTPUT, OFFSET STR7
CALL PRINTF
CALL SCANF 
CALL YNTO10
MOV AL, INPUT
;

CMP AL, 0
JE second_year
CALL EXTRA_RULE

second_year:
;READ WANNA USE ADDITIONAL_RULE
MOV OUTPUT, OFFSET STR8
CALL PRINTF
CALL SCANF 
CALL YNTO10
MOV AL, INPUT
;
CMP AL, 0
JE first_year
CALL ADDITIONAL_RULE

first_year:

CALL COMPUTE_SCORE

end_student:

MOV AL, FINAL_SCORE
CMP AL, 18
JB not_pass
CMP AL, 30
JA not_pass

;OUTPUT->GRADE
MOV OUTPUT, OFFSET STR9
CALL PRINTF     

MOV AL, FINAL_SCORE
MOV BL, 10
DIV BL

ADD AL, 48
MOV OUTC, AL
CALL PUTC 
 
ADD AH, 48
MOV OUTC, AH
CALL PUTC

MOV AL, HONOR_FLAG
CMP AL, 0
JE no_honor 
MOV OUTPUT, OFFSET STR10
CALL PRINTF

no_honor:
JMP after ; ERRORE???


not_pass:

;OUTPUT->REJECTED!
MOV OUTPUT, OFFSET STR11
CALL PRINTF
;


after: 

; PRINT A \r\n FOR READABILITY SAKE
MOV OUTC, 10
CALL PUTC
MOV OUTC, 13
CALL PUTC


INC BP
CMP BP, STUD
JNE loop_label

end_program:

.EXIT



DRAW PROC
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI  
   
    MOV SI, DECK_INDEX
    MOV DI, CARDS_INDEX
    MOV AL, DECKS[SI]
    
    MOV OUTPUT, OFFSET STR5
    CALL PRINTF
    
    AND AL, MASKV
    MOV CARDS[DI], AL 
     
    
    ;IF SERIES FOR CLOTHED CARDS
     CMP AL, 1
     JE ace
     CMP AL, 11
     JE jack
     CMP AL, 12
     JE queen
     CMP AL, 13
     JE king
     
     ; IF I LAND HERE I'M A NORMAL CARD
     ADD AL, 48 
     JMP go_print 
     
     ace: 
     MOV AL, 'A'
     JMP go_print
     
     jack:
     MOV AL, 'J'
     JMP go_print
     
     queen:
     MOV AL, 'Q'
     JMP go_print
     
     king:
     MOV AL, 'K'
     JMP go_print
      
    
    ;
    
    go_print:
    MOV OUTC, AL
  
    CALL PUTC
    
    MOV AL, DECKS[SI]
    AND AL, MASKS
    MOV CL, 4
    SHR AL, CL   ;ERRORE??  
    ADD AL, 3 ;PRINT THE SEED
    
    MOV OUTC, AL
    
    CALL PUTC
    
    
    
    INC DI
    MOV CARDS_INDEX, DI
    
    INC SI
    MOV DECK_INDEX, SI
    

    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX   
    RET        ; ERRORE!!! MANCA IL RET
    DRAW ENDP         

COMPUTE_SCORE PROC 
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI    
    
    MOV SI, CARDS_INDEX
    DEC SI
    XOR AX, AX   
    
    loop_compute_score:
    
    ADD AL, CARDS[SI]
    CMP SI, 0
    JE exit_this_loop
    
    DEC SI
    JMP loop_compute_score
    
    exit_this_loop:
    
    MOV BL, SUPER_FLAG
    CMP BL, 0
    JE no_super_rule      
    
    MOV CL, 52
    SUB CL, AL
    MOV FINAL_SCORE, CL
    MOV HONOR_FLAG, 0       
    JMP end_computation   
    
    
    no_super_rule:    
    ADD AL, 6
    MOV FINAL_SCORE, AL
    MOV HONOR_FLAG, 0
    
    end_computation:
    
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX 
    RET ;ERRORE?   
    COMPUTE_SCORE ENDP

SUPER_RULE PROC    
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI 
    
    MOV SUPER_FLAG, 1        ; EH EH EH
    
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX          
    RET
    SUPER_RULE ENDP

EXTRA_RULE PROC    
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI
    
    MOV SI, CARDS_INDEX
    loop_extra_rule: 
    
    DEC SI      ;ERROR!! IL DEC SI VA PRIMA DELLA LETTURA IN AL!! (ALTRIMENTI PARTI DA 1 IN PIU' MA FINISCI 1 PRIMA)
    
    MOV AL, CARDS[SI]
    CMP AL, 1
    JNE next_extra_rule
    MOV AL, 14
    next_extra_rule:
    MOV CARDS[SI], AL
    ;DEC SI     RIMOSSO
    CMP SI, 0
    JNE loop_extra_rule
    
    
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX   
    RET
    EXTRA_RULE ENDP

ADDITIONAL_RULE PROC    
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI  
    
    MOV SI, CARDS_INDEX
    ;DEC SI ;ERRORE!!! ANCHE QUI, IL DEC SI VA SEMPLICEMENTE DENTRO IL LOOP, COME PRIMA ISTRUZIONE!
    
    loop_additional:
    
    DEC SI  ;ORA E' GIUSTO
    
    MOV AL, CARDS[SI]
    CMP AL, 10
    JBE next_add
    
    CMP AL, 14
    JE next_add
    MOV AL, 10
    
    next_add:
    MOV CARDS[SI], AL
    ;DEC SI  RIMOSSO
    CMP SI, 0
    JNE loop_additional
    
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX     
    RET
    ADDITIONAL_RULE ENDP

CHECK_LUCK PROC    
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI    
    
    MOV SI, CARDS_INDEX
    ;CMP SI, 4           ;ERRORE!!! CON 3 CARTE VALE 3 (HO INIZIATO A CONTARE DA 1 INVECE CHE DA 0...) 
    CMP SI, 3
    JNE go_out_check_luck
    ;DEC SI              ;ERRORE! DEC SI VA DENTRO IL LOOP, NON FUORI (COME GLI ALTRI)
    check_luck_loop:    
    DEC SI               ;ORA E' GIUSTO
    MOV AL, CARDS[SI]
    CMP AL, 7
    JNE go_out_check_luck
    ;DEC SI             RIMOSSO
    CMP SI, 0
    JNE check_luck_loop:
    
    MOV LUCKY_FLAG, 1
    JMP end_check_luck
    
    go_out_check_luck:
    MOV LUCKY_FLAG, 0
    
    end_check_luck:
    
    
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX 
    RET  
    CHECK_LUCK ENDP  


PRINTF PROC 
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI 

    
    MOV AH, 09h   
    MOV DX, OUTPUT
    INT 21h
    
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX
    RET
    PRINTF ENDP  

SCANF PROC   
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI
    
    MOV AH,01h      ;READ 1 CHAR   
             
    INT 21h   
    
    SUB AL, 48
    MOV INPUT, AL
        
        
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX    
        
    RET
    SCANF ENDP 

PUTC PROC 
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI
    
    MOV AH,02h      ;WRITE 1 CHAR   
    
    MOV DL, OUTC
             
    INT 21h   
        
        
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX    
        
    RET
PUTC ENDP


YNTO10 PROC
    PUSH AX
    PUSH BX
    PUSH CX
    PUSH DX
    PUSH SI
    PUSH DI
    
    MOV AL, INPUT
    CMP AL, 41 ; BECAUSE SCANF ALREADY PERFORMS A -48
    JNE noY
    MOV AL, 1
    JMP endinputto10 
    noY:
    MOV AL, 0
    endinputto10: 
    MOV INPUT, AL
           
    POP DI
    POP SI
    POP DX
    POP CX
    POP BX
    POP AX    
        
    RET
    
    YNTO10 ENDP



END 